# Модификация стандартного образа, добавляющая двух дополнительных пользователей

Сборка из текущей директории:
```shell script
docker build -t registry.gitlab.com/fedyanint/x5-composite .
docker push registry.gitlab.com/fedyanint/x5-composite
```
Сборка из корня проекта
```shell script
docker build -t registry.gitlab.com/fedyanint/x5-composite/postgres:11.2 src/main/docker/postgres-two-users/ --no-cache
docker push registry.gitlab.com/fedyanint/x5-composite/postgres:11.2
```