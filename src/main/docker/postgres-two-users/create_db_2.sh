#!/bin/bash
set -e

POSTGRES="psql --username ${POSTGRES_USER}"

echo "Creating database: ${POSTGRES_USER_2}"

$POSTGRES <<EOSQL
CREATE DATABASE "${POSTGRES_USER_2}" OWNER "${POSTGRES_USER_2}";
EOSQL