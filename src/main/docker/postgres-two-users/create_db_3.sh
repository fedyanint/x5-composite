#!/bin/bash
set -e

POSTGRES="psql --username ${POSTGRES_USER}"

echo "Creating database: ${POSTGRES_USER_3}"

$POSTGRES <<EOSQL
CREATE DATABASE "${POSTGRES_USER_3}" OWNER "${POSTGRES_USER_3}";
EOSQL